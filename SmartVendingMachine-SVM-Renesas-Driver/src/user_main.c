/*
 * user_main.c
 *
 *  Created on: 28 May 2022
 *      Author: Trung
 */

#include "main.h"

uint16_t ADCx_GetValue(adc_ctrl_t * adcx_ctrl, adc_channel_t channel)
{
    uint16_t adc_result = 0;
    R_ADC_ScanStart(adcx_ctrl);
    adc_status_t status;
    status.state = ADC_STATE_SCAN_IN_PROGRESS;
    while(status.state == ADC_STATE_SCAN_IN_PROGRESS)
    {
        (void)R_ADC_StatusGet(adcx_ctrl, &status);
    }
    R_ADC_Read(adcx_ctrl, channel, &adc_result);
    return adc_result;
}
