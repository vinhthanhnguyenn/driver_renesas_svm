/*
 * user_main.h
 *
 *  Created on: May 7, 2022
 *      Author: buivi
 */

#ifndef USER_MAIN_H_
#define USER_MAIN_H_

#define DEMO

#include "hal_data.h"
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct task{
    bool Task_active;
    char Task_status;
    unsigned int Task_timeout;
}Task;

enum dropss{
    dropss_nodrop,
    dropss_valid,
    dropss_invalid
};

enum task_Status{
    task_status_stop,
    task_status_doing,
    task_status_error
};

enum task_ID{
    task_Diagnose,
    task_SlotReset,
    task_ReadTemp,
    task_Comb_One_Single,
    task_Comb_One_Double,
    task_Comb_all_Single,
    task_Settemp,
    task_Settime_Defrost,
    task_Settime_Working,
    task_Settime_Down,
    task_Light,
    task_Mode_OneBelt,
    task_Mode_OneSpring,
    task_Mode_AllBelt,
    task_Mode_AllSpring,
    task_Product_Dispense
};

enum cmd_And2Con
{
    cmd_Start,
    cmd_Serial,
    cmd_Serial_buf,
    cmd_Code1,
    cmd_Code1_buf,
    cmd_Code2,
    cmd_Code2_buf,
    cmd_Stop
};

enum cmd_Con2And
{
    rep_Serial=0,
    rep_Fault,
    rep_Code,
    rep_Dispense,
    rep_Checksum
};

#define CMD_START (unsigned char)0xAA
#define CMD_DIAGNOSE (unsigned char)0x64
#define CMD_SLOTRESET (unsigned char)0x65
#define CMD_READTEMP (unsigned char)0xDC
#define CMD_COMB_ONE_SINGLE (unsigned char)0xC9
#define CMD_COMB_ONE_DOUBLE (unsigned char)0xCA
#define CMD_COMB_ALL_SINGLE (unsigned char)0xCB
#define CMD_SETTEMP (unsigned char)0xCF
#define CMD_SETTIME_DEFROST (unsigned char)0xD1
#define CMD_SETTIME_WORKING (unsigned char)0xD2
#define CMD_SETTIME_DOWN (unsigned char)0xD3
#define CMD_LIGHT (unsigned char)0xDD
#define CMD_MODE_ONE_BELT (unsigned char)0x68
#define CMD_MODE_ONE_SPRING (unsigned char)0x74
#define CMD_MODE_ALL_BELT (unsigned char)0x76
#define CMD_MODE_ALL_SPRING (unsigned char)0x75
#define CMD_STOP 0x55

#define CMD2_DISP_NOSENSOR (unsigned char)0x55
#define CMD2_DISP_SENSOR (unsigned char)0xAA
#define CMD2_LIGHT_ON (unsigned char) 0xAA
#define CMD2_LIGHT_0FF (unsigned char) 0x55
#define CMD2_DEFAULT (unsigned char)0x55

#define REP_B1_NORMAL 0x5D
#define REP_B1_FAIL 0x5C

#define MACHINE_TRACE_SLOT 10
#define CNT_LED_TH 1000


void Task_init(Task *task);
bool check_Time(unsigned int cnt_local, unsigned int cnt_global, unsigned int thresholded);
bool check_TaskActive(Task *task, char begin);
bool check_Mottor_Toogle(unsigned int ADC_value, unsigned int th_down);
void Con2And_send(unsigned int *and2con, unsigned char Serial, unsigned char Fault, unsigned char Code,
        unsigned char Dispense, bool drop);
void gpio_toggle_bit(ioport_ctrl_t *p_ctrl, bsp_io_port_pin_t pin);
void UARTx_Send(uart_ctrl_t * uartx_ctrl, char * p_src, volatile uint8_t * uartx_event);
uint16_t ADC_GetValue(adc_ctrl_t * adcx_ctrl, adc_channel_t channel);


#endif /* USER_MAIN_H_ */

























